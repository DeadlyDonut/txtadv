#include <iostream>
#include <string>
#include <cxxopts.hpp>
#include <ncurses.h>
#include "screen.h"

using namespace std;
using namespace cxxopts;

int srow = 1, scol = 1;   //initial position
int mx_x = 79, mx_y = 23;
int mn_x = 0, mn_y = 0;   //minimumn x and y 
char player_char = '@', rep_char = '#';   //player char & "replacement" char
bool spawnoninit = true;

Screen sc1;

cxxopts::Options arguments("txtadv -p 'char' -r int -c int]", "Add description of program here");

void char_erase(int x, int y) { mvaddch(x, y, ' '); }

void initargs() {
    arguments.add_options("Standard args")
            ("h,help", "Print help")
            ("p,player", "Specify char used for player character", cxxopts::value<char>()
                ->default_value("@")->implicit_value("@"))
            ("r,row", "initial x starting value", cxxopts::value<int>()
                ->default_value("1")->implicit_value("1"))
            ("c,column", "initial y starting value", cxxopts::value<int>()
                ->default_value("1")->implicit_value("1"))
            /*("minx", "minimum x value for player", cxxopts::value<int>()
                ->default_value("0")->implicit_value("0"))
            ("miny", "minimum y value for player", cxxopts::value<int>()
                ->default_value("0")->implicit_value("0"))*/
            /*("maxx", "maximum x value for player", cxxopts::value<int>()
                ->default_value("23")->implicit_value("23"))
            ("maxy", "maximum y value for player", cxxopts::value<int>()
                ->default_value("79")->implicit_value("79"))*/
        ;
}

int main(int argc, char *argv[]) {
    try {
        
        initargs();

        arguments.parse(argc, argv);

        if(arguments.count("h")) {
            std::cout << arguments.help({"", "Standard args"}) << std::endl;
            exit(0);
        }

        if(arguments.count("p")) {
            player_char = arguments["p"].as<char>();
            } /*else {
                player_char = arguments["p"].as<char>();
                std::cout << "error parsing player char arg" << std::endl;
                exit(0);
        }*/

        //Note: -r and -c must come after -p in order to work
        if(arguments.count("r")) {
            srow = arguments["r"].as<int>();
            } /*else {
                srow = arguments["r"].as<int>();
                std::cout << "error parsing row arg" << std::endl;
                exit(0);
        } */

        if(arguments.count("c")) {
            scol = arguments["c"].as<int>();
            } /*else {
                scol = arguments["c"].as<int>();
                std::cout << "error parsing column arg" << std::endl;
                // exit(0);
        }*/

        /*
            Below arguments break program and have 
            been commented out until the can be fixed
        */
        /*if(arguments.count("minx")) {
            mn_x = arguments["minx"].as<int>();
            } else {
                std::cout << "error parsing min-x arg" << std::endl;
                exit(0);
        }

        if(arguments.count("miny")) {
            mn_y = arguments["miny"].as<int>();
            } else {
                std::cout << "error parsing min-y arg" << std::endl;
                exit(0);
        }*/

        /*if(arguments.count("maxx")) {
            mx_x = arguments["maxx"].as<int>();
            } else {
                mx_x = arguments["maxx"].as<int>();
                std::cout << "error parsing max-x arg" << std::endl;
                //exit(0);
        }

        if(arguments.count("maxy")) {
            mx_x = arguments["maxx"].as<int>();
            } else {
                mx_x = arguments["maxx"].as<int>();
                std::cout << "error parsing max-y arg" << std::endl;
                //exit(0);
        }*/

    } catch (const cxxopts::OptionException& e)
        {
            std::cout << "error parsing options: " << e.what() << std::endl;
            exit(1);
        }

    // Start ncurses
    sc1.initbnd(mn_x, mn_y, mx_x, mx_y);
    sc1.initchar(player_char, srow, scol);

    sc1.init(spawnoninit);

    // Start the game loop
    if(sc1.getbool("initspawn") == false) {
        mvaddch(sc1.getint("row"), sc1.getint("col"), sc1.getchar("player"));
        refresh();
    } else refresh();

    for(;;){
        int key = sc1.getint("ch");
        key = getch();   

        /*
            Keyboard input to move in a 
            specific direction with the arrow keys
        */
        if(key == KEY_LEFT) {
            char_erase(sc1.getint("row"), sc1.getint("col"));
            sc1.setpos(sc1.getint("row"), sc1.getint("col") - 1);
        }
        else if(key == KEY_RIGHT) {
            char_erase(sc1.getint("row"), sc1.getint("col"));
            sc1.setpos(sc1.getint("row"), sc1.getint("col") + 1);
        }
        else if(key == KEY_UP) {
            char_erase(sc1.getint("row"), sc1.getint("col"));
            sc1.setpos(sc1.getint("row") - 1, sc1.getint("col"));
        }
        else if(key == KEY_DOWN) {
            char_erase(sc1.getint("row"), sc1.getint("col"));
            sc1.setpos(sc1.getint("row") + 1, sc1.getint("col"));
        }

        /*
            protected Scene function to limit where 
            the character (/player character) can travel 
        */
        sc1.limit();

        /*
            Character is now ready to render. Shift 
            the character and refresh the terminal
        */
        mvaddch(sc1.getint("row"), sc1.getint("col"), sc1.getchar("player"));
        refresh();
    }

    // Clear ncurses data structures
    endwin();
    return 0;
}