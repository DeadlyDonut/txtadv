txtadv

Requires ncurses from mingw-64-x86_64/mingw-w64-i686 and ncurses-devel from the same package in MSYS2.
Also requires https://github.com/jarro2783/cxxopts/

TODO:
Priorities (High to low): 1-4

P(1)
sceen.h/.cpp (& main.cpp)
	game_loop
		Now integrated into main.cpp (needs some cleaning up)
	cxxopts lib
		tweaks needed involving arguments

P(3)
main.cpp/ & screen.h/.cpp
	Experiment with coloring text
	LONGTERM: Experiment with loading text into ncurses to use as a background

P(4)
platform
	Port to other terminal applications/Linux someday.