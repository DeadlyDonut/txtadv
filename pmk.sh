#!/bin/bash
STRINIT="Make debug, release, both or clean [d/r/b/c]?"
STRSTRT="Making: "
STRDBG="debug version of the application"
STRREL="release version of the application"
STRBTH="both versions of the application"
STRCLN="Cleaning the project directory"
STREXE="txtadv.exe"
VALID=0
if [[ "$1" = "" ]]; then
	echo $STRINIT
	while [ $VALID = 0 ]; do
		read INPUT
		if [ "$INPUT" = "d" ]; then
			make debug
			./run.sh
			VALID=1
		elif [ "$INPUT" = "r" ]; then
			make
			./run.sh
			VALID=1
		elif [ "$INPUT" = "b" ]; then
			make
			make debug
			VALID=1
		elif [ "$INPUT" = "c" ]; then
			make clean
			VALID=1
		else
			echo Invalid input
			echo $STRINIT
		fi
	done

	elif [[ "$1" = "d" ]] || [[ "$1" = "r" ]] || [[ "$1" = "b" ]] || [[ "$1" = "c" ]]; then
		if [ "$1" = "d" ]; then
			echo $STRSTRT $STRDBG	
			sleep 1
			make debug
			./run.sh $2 $3 $4 $5 $6 $7
		elif [ "$1" = "r" ]; then
			echo $STRSTRT $STRREL
			sleep 1
			make
			./run.sh $2 $3 $4 $5 $6 $7
		elif [ "$1" = "b" ]; then
			echo $STRSTRT $STRBTH
			sleep 1
			make
			make debug
		elif [ "$1" = "c" ]; then
			echo $STRCLN
			make clean
			sleep 1
		fi
fi