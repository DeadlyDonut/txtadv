#include <ncurses.h>
#include "screen.h"

int Screen::row = 0, Screen::col = 0, Screen::ch = 0;
int Screen::min_x = 0, Screen::min_y = 0;
int Screen::max_x = 0, Screen::max_y = 0; 

char Screen::player = ' ';
bool Screen::initspawn;

//Constructor for Screen class
Screen::Screen()
{
}  

/*
    Seperation of "construction" of the screen class 
    so that minx/y and max-x/y can be modified on the fly
*/
void Screen::initbnd(int mn_x, int mn_y, int mx_x, int mx_y)
{
    max_x = mx_x;
    max_y = mx_y;
    min_x = mn_x;
    min_y = mn_y;
}

/*
    Seperation of "construction" of the 
    screen class so that the main_char and 
    starting x and y values can be modified on the fly
*/
void Screen::initchar(char main_char, int x, int y)
{
    player = main_char;
    row = x;
    col = y;
}

/* 
    Initialize the ncurses library
    Allows user to decide if char is 
    placed when screen is inited
*/
void Screen::init(bool spawn)
{
    initspawn = spawn; 
    initscr();
    clear();
    noecho();
    cbreak();
    keypad(stdscr, TRUE);
    curs_set(0);
    if(initspawn == true)
    {
        mvaddch(row, col, player); // Show the main character on the screen
        refresh();
    }
}	

/* 
	Defines the "erase" character, to "clean" the path 
    of the game character, can be a whitespace 
    char or the defined replacement char
*/
void Screen::erase(int x, int y) { mvaddch(x, y, ' '); } 
//add whitespace char to replace player char

/*
    Retrieves a specified int value 
    from an instance of the Scene 
    class. getchar can only retrieve 
    chars specified in getbool.
*/
int Screen::getint(string var)
{
    if(var == "row") return Screen::row;
    if(var == "col") return Screen::col;
    if(var == "ch") return Screen::ch;
    if(var == "min_x") return Screen::min_x;
    if(var == "min_y") return Screen::min_y;
    if(var == "max_x") return Screen::max_x;
    if(var == "max_y") return Screen::max_y;
    else return NULL;
}

/*
    Retrieves a specified char value 
    from an instance of the Scene 
    class. getchar can only retrieve 
    chars specified in getbool.
*/
char Screen::getchar(string var)
{
    if(var == "player" || var == "main_char") return Screen::player;
    else return NULL;
}

/*
    Retrieves a specified boolean value 
    from an instance of the Scene class.
    getbool can only retrieve strings 
    specified in getbool.
*/
bool Screen::getbool(string var)
{
    if(var == "initspawn" || var == "spawn") return Screen::initspawn;
    else return NULL;
}

/*
    setpos changes the internal 
    x and y (aka row and col) values  
*/
void Screen::setpos(int x, int y)
{
    Screen::row = x;
    Screen::col = y;
}

/*
    Function to define 
    basic screen boundaries
*/
void Screen::limit()
{
	if(col+1 >= max_x)
    {
    	erase(row, col);
    	col = max_x-1;
    	erase(row, col);
    }
    else if(col-1 <= min_x)
    {
    	erase(row, col);
    	col = min_x+1;
    	erase(row, col);
    }

    if(row+1 >= max_y)
    {
    	erase(row, col);
    	row = max_y-1;
    	erase(row, col);
    }
    else if(row-1 <= min_y)
    {
    	erase(row, col);
    	row = min_y + 1;
    	erase(row, col);
    }
}

//Easy game loop setup
void Screen::game_loop()
{
    // Show the main character on the screen
    if(!initspawn) {
        mvaddch(row, col, player);
        refresh();
    }
    
    refresh();

    for(;;){
        ch = getch();   

        /*
            Keyboard input to move in a 
            specific direction with the arrow keys
        */
        if(ch == KEY_LEFT) {
            erase(row, col);
            col = col - 1;
        }
        else if(ch == KEY_RIGHT) {
            erase(row, col);
            col = col + 1;
        }
        else if(ch == KEY_UP) {
            erase(row, col);
            row = row - 1;
        }
        else if(ch == KEY_DOWN) {
            erase(row, col);
            row = row + 1; 
        }

        /*
            protected Scene function to limit where 
            the character (/player character) can travel 
        */
        limit();

        /*
            Character is now ready to render. Shift 
            the character and refresh the terminal
        */
        mvaddch(row, col, player);
        refresh();
    }
}