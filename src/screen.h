#ifndef SCREEN_H	
#define SCREEN_H

#include <ncurses.h>
#include <string>

using namespace std;

class Screen {

	public:
		Screen();
		void initbnd(int mn_x, int mn_y, int mx_x, int mx_y);
		void initchar(char player, int x, int y);
		void init(bool spawn);
		int getint(string var);
		char getchar(string var);
		bool getbool(string var);
		void setpos(int x, int y);
		void limit();
		void game_loop();
		static bool initspawn;
		static char player;
		static int row, col, ch;
		static int max_x, max_y;
		static int min_x, min_y;
	protected:
		void erase(int x, int y);
		
	/*
	 	TODO define specified functions 
	 	in later release candidate
	*/ 
	/*
		void setmin(int x, int y);
		void setmax(int x, int y);
		void setchar(char nchar);
	*/
	private:		
};

#endif